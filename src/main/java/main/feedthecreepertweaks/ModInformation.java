package main.feedthecreepertweaks;

/*
 * Basic information your mod depends on.
 */

public class ModInformation
{

   public static final String NAME = "Feed the Creeper Tweaks";
   public static final String ID = "feedthecreepertweaks";
   public static final String CHANNEL = "FeedTheCreeperTweaks";
   public static final String DEPEND = "required-after:TConstruct;required-after:ExtraUtilities;required-after:BiomesOPlenty;required-after:Thaumcraft;required-after:Highlands;required-after:progressiveautomation;required-after:BuildCraft|Energy";
   public static final String VERSION = "1.1.0";
   public static final String CLIENTPROXY = "main.feedthecreepertweaks.proxies.ClientProxy";
   public static final String COMMONPROXY = "main.feedthecreepertweaks.proxies.CommonProxy";;
}
